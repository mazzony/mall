<?php

namespace App\Http\Controllers;
use App\Models\StoreCategory;
use Illuminate\Http\Request;

class StoreCategoryController extends Controller
{
    public function index(){
        return view('store_category.index');
    }
   
    public function create(){
        return view('store_category.create');

    }
    public function store(Request $request){
      
        StoreCategory::create([
            'name'=>$request->name,
         
        ]);
   
        return view('store_category.create');

    }
    public function get_all_categories(Request $request){
        $columns = array( 
            0 =>'id', 
            1 =>'name',
            2=> 'created_at',
            3=> 'id',
        );

$totalData = StoreCategory::count();

$totalFiltered = $totalData; 

$limit = $request->input('length');
$start = $request->input('start');
$order = $columns[$request->input('order.0.column')];
$dir = $request->input('order.0.dir');

if(empty($request->input('search.value')))
{            
$Adss = StoreCategory::offset($start)
         ->limit($limit)
         ->orderBy($order,$dir)
         ->get();
}
else {
$search = $request->input('search.value'); 

$Adss =  StoreCategory::where('id','LIKE',"%{$search}%")
            ->orWhere('title', 'LIKE',"%{$search}%")
            ->offset($start)
            ->limit($limit)
            ->orderBy($order,$dir)
            ->get();

$totalFiltered = StoreCategory::where('id','LIKE',"%{$search}%")
             ->orWhere('title', 'LIKE',"%{$search}%")
             ->count();
}

$data = array();
if(!empty($Adss))
{
foreach ($Adss as $Ads)
{
$show = '';
$edit = '' ;

$nestedData['id'] = $Ads->id;
$nestedData['name'] = $Ads->name;
$nestedData['created_at'] = date('j M Y h:i a',strtotime($Ads->created_at));
$nestedData['options'] = "&emsp;<a href='{$show}' title='SHOW' ><span class='glyphicon glyphicon-list'></span></a>
                          &emsp;<a href='{$edit}' title='EDIT' ><span class='glyphicon glyphicon-edit'></span></a>";
$data[] = $nestedData;

}
}

$json_data = array(
    "draw"            => intval($request->input('draw')),  
    "recordsTotal"    => intval($totalData),  
    "recordsFiltered" => intval($totalFiltered), 
    "data"            => $data   
    );

echo json_encode($json_data); 

    }
}
