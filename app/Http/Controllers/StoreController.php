<?php

namespace App\Http\Controllers;
use App\Models\Stores;
use App\Models\StoreCategory;
use App\Models\collection;

use Illuminate\Http\Request;

class StoreController extends Controller
{
    public function index(){
        return view('store.index');
    }
   
    public function create(){
        $categories = StoreCategory::all();
        $collections = collection::all();

        return view('store.create',compact('categories','collections'));

    }
    public function store(Request $request){

        $arr = $request->section;
        $sections_string =  implode(",",$arr);
        
        $imageName = time().'.'.$request->image->extension();  
   
        $request->image->move(public_path('images'), $imageName);

        
     


        Stores::create([
            'name'=>$request->name,
            'banner'=>$imageName ,
            'category_id'=>$request->category,
            'collection_id'=>$request->collection,
            'sections'=>$sections_string
        ]);
   
        return redirect('/store/create');

    }
    public function get_all_stores(Request $request){

    
        $columns = array( 
            0 =>'id', 
            1 =>'name',
            2=> 'category',
            3=> 'collection',
            4=> 'created_at',
            4=> 'id',
        );

$totalData = Stores::count();

$totalFiltered = $totalData; 

$limit = $request->input('length');
$start = $request->input('start');
$order = $columns[$request->input('order.0.column')];
$dir = $request->input('order.0.dir');

if(empty($request->input('search.value')))
{            
$Adss = Stores::offset($start)
         ->limit($limit)
         ->orderBy($order,$dir)
         ->with(['category','collection'])
         ->get();
}
else {
$search = $request->input('search.value'); 

$Adss =  Stores::where('id','LIKE',"%{$search}%")
            ->orWhere('name', 'LIKE',"%{$search}%")
            ->offset($start)
            ->limit($limit)
            ->orderBy($order,$dir)
            ->with(['category','collection'])
            ->get();

$totalFiltered = Stores::where('id','LIKE',"%{$search}%")
             ->orWhere('name', 'LIKE',"%{$search}%")
             ->with(['category','collection'])
             ->count();
}

$data = array();
if(!empty($Adss))
{
foreach ($Adss as $Ads)
{
$show = '';
$edit = '' ;

$nestedData['id'] = $Ads->id;
$nestedData['name'] = $Ads->name;
$nestedData['collection'] = $Ads->collection->name;
$nestedData['category'] = $Ads->category->name;
$nestedData['created_at'] = date('j M Y h:i a',strtotime($Ads->created_at));
$nestedData['options'] = "&emsp;<a href='{$show}' title='SHOW' ><span class='glyphicon glyphicon-list'></span></a>
                          &emsp;<a href='{$edit}' title='EDIT' ><span class='glyphicon glyphicon-edit'></span></a>";
$data[] = $nestedData;

}
}

$json_data = array(
    "draw"            => intval($request->input('draw')),  
    "recordsTotal"    => intval($totalData),  
    "recordsFiltered" => intval($totalFiltered), 
    "data"            => $data   
    );

echo json_encode($json_data); 

    }
    public function get_number_of_sections($id){
        $collection = collection::find($id);
        return $collection->number_of_sections;

    }
}
