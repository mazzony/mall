<?php

namespace App\Http\Controllers;
use App\Models\Ads;
use Illuminate\Http\Request;

class AdsController extends Controller
{
    public function index(){
        return view('ads.index');
    }
   
    public function create(){
        return view('ads.create');

    }
    public function store(Request $request){
        $imageName = time().'.'.$request->image->extension();  
   
        $request->image->move(public_path('images'), $imageName);
        //dd($imageName );
        Ads::create([
            'name'=>$request->name,
            'img'=>$imageName ,
            'description'=>$request->description
        ]);
   
        return view('ads.create');

    }
    public function get_all_ads(Request $request){
        $columns = array( 
            0 =>'id', 
            1 =>'title',
            2=> 'body',
            3=> 'created_at',
            4=> 'id',
        );

$totalData = Ads::count();

$totalFiltered = $totalData; 

$limit = $request->input('length');
$start = $request->input('start');
$order = $columns[$request->input('order.0.column')];
$dir = $request->input('order.0.dir');

if(empty($request->input('search.value')))
{            
$Adss = Ads::offset($start)
         ->limit($limit)
         ->orderBy($order,$dir)
         ->get();
}
else {
$search = $request->input('search.value'); 

$Adss =  Ads::where('id','LIKE',"%{$search}%")
            ->orWhere('title', 'LIKE',"%{$search}%")
            ->offset($start)
            ->limit($limit)
            ->orderBy($order,$dir)
            ->get();

$totalFiltered = Ads::where('id','LIKE',"%{$search}%")
             ->orWhere('title', 'LIKE',"%{$search}%")
             ->count();
}

$data = array();
if(!empty($Adss))
{
foreach ($Adss as $Ads)
{
$show = '';
$edit = '' ;

$nestedData['id'] = $Ads->id;
$nestedData['name'] = $Ads->name;
$nestedData['description'] = $Ads->description;
$nestedData['created_at'] = date('j M Y h:i a',strtotime($Ads->created_at));
$nestedData['options'] = "&emsp;<a href='{$show}' title='SHOW' ><span class='glyphicon glyphicon-list'></span></a>
                          &emsp;<a href='{$edit}' title='EDIT' ><span class='glyphicon glyphicon-edit'></span></a>";
$data[] = $nestedData;

}
}

$json_data = array(
    "draw"            => intval($request->input('draw')),  
    "recordsTotal"    => intval($totalData),  
    "recordsFiltered" => intval($totalFiltered), 
    "data"            => $data   
    );

echo json_encode($json_data); 

    }
}
