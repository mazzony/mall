<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Stores;
use App\Models\Product;

class ProductController extends Controller
{
    public function index(){
        return view('product.index');
    }
   
    public function create(){
        $stores = Stores::all();
        

        return view('product.create',compact('stores'));

    }
    public function store(Request $request){

     
        
        $imageName = time().'.'.$request->image->extension();  
   
        $request->image->move(public_path('images'), $imageName);

        $size='no';
        $color = 'no';
     
        if($request->has_attributes =="1"){
            $size=$request->color;
        $color = $request->size;

        }

        Product::create([
            'name'=>$request->name,
            'img'=>$imageName ,
            'store_id'=>$request->store_id,
            'status'=>$request->status,
            'price'=>$request->price,
            'stock'=>$request->stock,
            'size'=>$size,
            'color'=>$color,
            'has_attributes'=>$request->has_attributes ,
           


           
           
        ]);
   
        return redirect('/product/create');

    }
    public function get_all_products(Request $request){

    
        $columns = array( 
            0 =>'id', 
            1 =>'name',
            2=> 'store',
            3=> 'status',
            4=> 'price',
            5=> 'stock',
            6=> 'created_at',
            7=> 'id',
        );

$totalData = Product::count();

$totalFiltered = $totalData; 

$limit = $request->input('length');
$start = $request->input('start');
$order = $columns[$request->input('order.0.column')];
$dir = $request->input('order.0.dir');

if(empty($request->input('search.value')))
{            
$Adss = Product::offset($start)
         ->limit($limit)
         ->orderBy($order,$dir)
         ->with(['store'])
         ->get();
}
else {
$search = $request->input('search.value'); 

$Adss =  Product::where('id','LIKE',"%{$search}%")
            ->orWhere('name', 'LIKE',"%{$search}%")
            ->offset($start)
            ->limit($limit)
            ->orderBy($order,$dir)
            ->with(['store'])
            ->get();

$totalFiltered = Product::where('id','LIKE',"%{$search}%")
             ->orWhere('name', 'LIKE',"%{$search}%")
             ->with(['store'])
             ->count();
}

$data = array();
if(!empty($Adss))
{
foreach ($Adss as $Ads)
{
$show = '';
$edit = '' ;

$nestedData['id'] = $Ads->id;
$nestedData['name'] = $Ads->name;
$nestedData['store'] = $Ads->store->name;
$status='';
if($Ads->status==0){
$status='Not Active';
}else{
    $status=' Active';
}
$nestedData['status'] = $status;
$nestedData['price'] = $Ads->price;
$nestedData['stock'] = $Ads->stock;
$nestedData['created_at'] = date('j M Y h:i a',strtotime($Ads->created_at));
$nestedData['options'] = "&emsp;<a href='{$show}' title='SHOW' ><span class='glyphicon glyphicon-list'></span></a>
                          &emsp;<a href='{$edit}' title='EDIT' ><span class='glyphicon glyphicon-edit'></span></a>";
$data[] = $nestedData;

}
}

$json_data = array(
    "draw"            => intval($request->input('draw')),  
    "recordsTotal"    => intval($totalData),  
    "recordsFiltered" => intval($totalFiltered), 
    "data"            => $data   
    );

echo json_encode($json_data); 

    }
    
}
