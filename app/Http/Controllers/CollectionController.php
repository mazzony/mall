<?php

namespace App\Http\Controllers;
use App\Models\collection;
use Illuminate\Http\Request;

class CollectionController extends Controller
{
    public function index(){
        return view('collection.index');
    }
   
    public function create(){
        return view('collection.create');

    }
    public function store(Request $request){
      
        collection::create([
            'name'=>$request->name,
            'description'=>$request->description,
            'number_of_sections'=>$request->number_of_sections,

         
        ]);
   
        return view('collection.create');

    }
    public function get_all_collections(Request $request){
        $columns = array( 
            0 =>'id', 
            1 =>'name',
            2 =>'description',
            3=>'number_of_sections',
            4=> 'created_at',
            5=> 'id',
        );

$totalData = collection::count();

$totalFiltered = $totalData; 

$limit = $request->input('length');
$start = $request->input('start');
$order = $columns[$request->input('order.0.column')];
$dir = $request->input('order.0.dir');

if(empty($request->input('search.value')))
{            
$Adss = collection::offset($start)
         ->limit($limit)
         ->orderBy($order,$dir)
         ->get();
}
else {
$search = $request->input('search.value'); 

$Adss =  collection::where('id','LIKE',"%{$search}%")
            ->orWhere('title', 'LIKE',"%{$search}%")
            ->offset($start)
            ->limit($limit)
            ->orderBy($order,$dir)
            ->get();

$totalFiltered = collection::where('id','LIKE',"%{$search}%")
             ->orWhere('title', 'LIKE',"%{$search}%")
             ->count();
}

$data = array();
if(!empty($Adss))
{
foreach ($Adss as $Ads)
{
$show = '';
$edit = '' ;

$nestedData['id'] = $Ads->id;
$nestedData['name'] = $Ads->name;
$nestedData['description'] = $Ads->description;
$nestedData['number_of_sections'] = $Ads->number_of_sections;
$nestedData['created_at'] = date('j M Y h:i a',strtotime($Ads->created_at));
$nestedData['options'] = "&emsp;<a href='{$show}' title='SHOW' ><span class='glyphicon glyphicon-list'></span></a>
                          &emsp;<a href='{$edit}' title='EDIT' ><span class='glyphicon glyphicon-edit'></span></a>";
$data[] = $nestedData;

}
}

$json_data = array(
    "draw"            => intval($request->input('draw')),  
    "recordsTotal"    => intval($totalData),  
    "recordsFiltered" => intval($totalFiltered), 
    "data"            => $data   
    );

echo json_encode($json_data); 

    }

    
}
