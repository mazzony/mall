<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table='products';
    protected $fillable =['name','img','status','stock','price','store_id','has_attributes','size','color'];


   
    public function store()
    {
        return $this->hasOne(Stores::class ,'id','store_id');
    }
}
