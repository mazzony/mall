<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stores extends Model
{
    use HasFactory;
    protected $table ='stores';
    protected $fillable =['name','banner','collection_id','category_id','sections'];

    public function category()
    {
        return $this->hasOne(StoreCategory::class , 'id','category_id');
    }

    public function collection()
    {
        return $this->hasOne(collection::class ,'id','collection_id');
    }
}
