<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Auth::routes();

Route::get('/home', 'App\Http\Controllers\HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'App\Http\Controllers\UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']);
	Route::get('upgrade', function () {return view('pages.upgrade');})->name('upgrade'); 
	 Route::get('map', function () {return view('pages.maps');})->name('map');
	 Route::get('icons', function () {return view('pages.icons');})->name('icons'); 
	 Route::get('table-list', function () {return view('pages.tables');})->name('table');
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\ProfileController@password']);
});
Route::get('/ads/create',  [App\Http\Controllers\AdsController::class, 'create']);
Route::get('/ads/index',  [App\Http\Controllers\AdsController::class, 'index']);
Route::post('/ads/store',  [App\Http\Controllers\AdsController::class, 'store']);
Route::post('/ads/all_ads',  [App\Http\Controllers\AdsController::class, 'get_all_ads']);

Route::get('/store_category/create',  [App\Http\Controllers\StoreCategoryController::class, 'create']);
Route::get('/store_category/index',  [App\Http\Controllers\StoreCategoryController::class, 'index']);
Route::post('/store_category/store',  [App\Http\Controllers\StoreCategoryController::class, 'store']);
Route::post('/store_category/all_categories',  [App\Http\Controllers\StoreCategoryController::class, 'get_all_categories']);


Route::get('/collection/create',  [App\Http\Controllers\CollectionController::class, 'create']);
Route::get('/collection/index',  [App\Http\Controllers\CollectionController::class, 'index']);
Route::post('/collection/store',  [App\Http\Controllers\CollectionController::class, 'store']);
Route::post('/collection/all_collections',  [App\Http\Controllers\CollectionController::class, 'get_all_collections']);


Route::get('/store/create',  [App\Http\Controllers\StoreController::class, 'create']);
Route::get('/store/index',  [App\Http\Controllers\StoreController::class, 'index']);
Route::post('/store/store',  [App\Http\Controllers\StoreController::class, 'store']);
Route::post('/store/all_stores',  [App\Http\Controllers\StoreController::class, 'get_all_stores']);
Route::get('/store/get_number_of_sections/{id}',  [App\Http\Controllers\StoreController::class, 'get_number_of_sections']);


Route::get('/product/create',  [App\Http\Controllers\ProductController::class, 'create']);
Route::get('/product/index',  [App\Http\Controllers\ProductController::class, 'index']);
Route::post('/product/store',  [App\Http\Controllers\ProductController::class, 'store']);
Route::post('/product/all_products',  [App\Http\Controllers\ProductController::class, 'get_all_products']);

